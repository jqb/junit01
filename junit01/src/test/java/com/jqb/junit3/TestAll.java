package com.jqb.junit3;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;
/***
 * 使用测试套件
 * @author Dong
 *
 */
public class TestAll extends TestCase{

	public static Test suite(){
		
		TestSuite suite = new TestSuite();
		/***
		 * 为测试套件对象添加测试类,批量测试
		 */
		suite.addTestSuite(DemoATest.class);
		suite.addTestSuite(DemoBTest.class);
		suite.addTestSuite(DemoCTest.class);
		
		return suite;
	}
}
