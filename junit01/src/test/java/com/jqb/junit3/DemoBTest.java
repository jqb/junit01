package com.jqb.junit3;

import com.jqb.junit3.DemoB;

import junit.framework.Assert;
import junit.framework.TestCase;

public class DemoBTest extends TestCase {
	DemoB demoB = null;

	@Override
	protected void setUp() throws Exception {
		demoB = new DemoB();
	}

	/**
	 * 测试正常数组
	 */
	public void testGetMaxOfArray() {
		int[] array = { 1, 2, 9, 4, 5, 6 };
		int max = 0;
		try {
			max = demoB.getMaxOfArray(array);
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail("测试失败");
		}
		Assert.assertEquals(9, max);
	}

	/**
	 * 测试空数组
	 */
	public void testGetMaxOfArrayEmpty() {
		int[] array = {};
		Throwable testException = null;
		try {
			demoB.getMaxOfArray(array);
			Assert.fail("测试失败");
		} catch (Exception e) {
			testException = e;
		}
		Assert.assertNotNull(testException);
		Assert.assertEquals(Exception.class, testException.getClass());
		Assert.assertEquals("数组不能为空", testException.getMessage());
	}

	/**
	 * 测试Null数组
	 */
	public void testGetMaxOfArrayNull() {
		int[] array = null;
		Throwable testException = null;
		try {
			demoB.getMaxOfArray(array);
			Assert.fail("测试失败");
		} catch (Exception e) {
			testException = e;
		}
		Assert.assertNotNull(testException);
		Assert.assertEquals(Exception.class, testException.getClass());
		Assert.assertEquals("数组不能为空", testException.getMessage());

	}
	/***
	 * junit自带的工具
	 */
	public static void main(String[] args){
		/***
		 * 命令行的方式测试(实际开发过程中一般用这种方式)
		 */
//		junit.textui.TestRunner.run(DemoBTest.class);
		/***
		 * awt图形界面
		 */
//		junit.awtui.TestRunner.run(DemoBTest.class);
		/***
		 * swing图形界面
		 */
		junit.swingui.TestRunner.run(DemoBTest.class);
	}
	
}
