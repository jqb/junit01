package com.jqb.junit3;

import junit.framework.Assert;
import junit.framework.TestCase;

public class DemoCTest extends TestCase {

	DemoC demoC = null;

	@Override
	protected void setUp() throws Exception {
		demoC = new DemoC();
	}

	/***
	 * 测试push 1个值
	 */
	public void testPush1() {
		String result = null;
		try {
			demoC.push("push");
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail("测试失败");
		}
		try {
			result = demoC.pop();
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail("测试失败");
		}
		Assert.assertEquals("push", result);
	}

	/***
	 * 测试push 100个值
	 */
	public void testPush2() {
		String result = null;
		try {
			for (int i = 0; i < 100; i++) {
				/* 存放1-99 */
				demoC.push(i + "");
			}
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail("测试失败");
		}
		try {
			for (int i = 99; i >= 0; i--) {
				result = demoC.pop();
				/* 先进后出,从99开始匹配 */
				Assert.assertEquals(i + "", result);
			}
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail("测试失败");
		}
	}

	/***
	 * 测试push 101个值
	 */
	public void testPush3() {
		Throwable testException = null;
		try {
			for (int i = 0; i <= 100; i++) {
				/* 存放1-99 */
				demoC.push(i + "");
			}
			Assert.fail("测试失败");
		} catch (Exception e) {
			testException = e;
			System.out.println(e.getMessage());
		}
		Assert.assertNotNull(testException);
		Assert.assertEquals(Exception.class, testException.getClass());
		Assert.assertEquals("已至栈顶", testException.getMessage());
	}

	/***
	 * 测试pop 1个值
	 */
	public void testPop1() {
		String result = null;
		try {
			demoC.push("pop");
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail("测试失败");
		}
		try {
			result = demoC.pop();
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail("测试失败");
		}
		Assert.assertEquals("pop", result);
	}

	/***
	 * 测试pop 100个值
	 */
	public void testPop2() {
		String result = null;
		try {
			for (int i = 0; i < 100; i++) {
				demoC.push(i + "");
			}
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail("测试失败");
		}
		try {
			for (int i = 99; i >= 0; i--) {
				result = demoC.pop();
				Assert.assertEquals(i + "", result);
			}
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail("测试失败");
		}
	}

	/***
	 * 测试pop 101个值
	 */
	public void testPop3() {
		Throwable testException = null;
		try {
			for (int i = 0; i < 100; i++) {
				demoC.push(i + "");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		try {
			for (int i = 0; i <= 100; i++) {
				demoC.pop();
			}
			Assert.fail("测试失败");
		} catch (Exception e) {
			testException = e;
			System.out.println(e.getMessage());
		}
		Assert.assertNotNull(testException);
		Assert.assertEquals(Exception.class, testException.getClass());
		Assert.assertEquals("已至栈底", testException.getMessage());
	}

	/***
	 * 测试delete 1个值
	 */
	public void testDelete1() {
		int index = 0;
		try {
			demoC.push("delete");
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail("测试失败");
		}
		try {
			index = demoC.delete(1);
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail("测试失败");
		}
		Assert.assertEquals(0, index);
	}

	/***
	 * 测试delete 100个值
	 */
	public void testDelete2() {
		int index = 0;
		try {
			for (int i = 0; i < 100; i++) {
				demoC.push("delete");
			}
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail("测试失败");
		}
		try {
			index = demoC.delete(100);
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail("测试失败");
		}
		Assert.assertEquals(0, index);
	}

	/***
	 * 测试delete 101个值
	 */
	public void testDelete3() {
		Throwable testException = null;
		try {
			for (int i = 0; i < 100; i++) {
				demoC.push("delete");
			}
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail("测试失败");
		}
		try {
			demoC.delete(101);
			Assert.fail("测试失败");
		} catch (Exception e) {
			testException = e;
			System.out.println(e.getMessage());
		}
		Assert.assertNotNull(testException);
		Assert.assertEquals(Exception.class, testException.getClass());
		Assert.assertEquals("栈中元素不足", testException.getMessage());
	}

	/***
	 * 测试获取栈顶元素(栈中有值)
	 */
	public void testTop1() {
		String result = null;
		try {
			demoC.push("top");
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail("测试失败");
		}
		try {
			result = demoC.top();
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail("测试失败");
		}
		Assert.assertEquals("top", result);
	}
	
	/***
	 * 测试获取栈顶元素(栈中无值)
	 */
	public void testTop2() {
		Throwable testException = null;
		try {
			demoC.top();
			Assert.fail("测试失败");
		} catch (Exception e) {
			testException = e;
			System.out.println(e.getMessage());
		}
		Assert.assertNotNull(testException);
		Assert.assertEquals(Exception.class,testException.getClass());
		Assert.assertEquals("栈是空的", testException.getMessage());
	}
}
