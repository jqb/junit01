package com.jqb.junit3;

import java.io.File;
import java.io.IOException;

import junit.framework.Assert;
import junit.framework.TestCase;

public class DemoDTest extends TestCase {
	/***
	 * 删除单个文件
	 */
	public void testDeleteAll1(){
		File file = new File("src/main/resources/1.txt");
		try {
			file.mkdirs();
			file.createNewFile();
			DemoD.deleteAll(file);
		} catch (IOException e) {
			e.printStackTrace();
			Assert.fail("测试失败");
		}
		Assert.assertEquals(false, file.exists());
	}
	/***
	 * 删除目录
	 * 目录结构：
	 * G: -bak -dl -x 1.txt 2.txt
	 *             -dir1 3.txt
	 *             -dir2 4.txt
	 */
	public void testDeleteAll2(){
		File dir = new File("src/main/resources/");
		try {
			dir.mkdirs();
			File file1 = new File(dir,"1.txt");
			File file2 = new File(dir,"2.txt");
			file1.createNewFile();
			file2.createNewFile();
			File dir1 = new File(dir,"dir1");
			File dir2 = new File(dir,"dir2");
			dir1.mkdir();
			dir2.mkdir();
			File file3 = new File(dir1,"3.txt");
			File file4 = new File(dir2,"4.txt");
			file3.createNewFile();
			file4.createNewFile();
			DemoD.deleteAll(dir);
		} catch (IOException e) {
			e.printStackTrace();
			Assert.fail("测试失败");
		}
		/***
		 * 文件夹是否为空
		 */
		Assert.assertEquals(0, dir.listFiles().length);
		/***
		 * 测试完擦屁股
		 */
		dir.delete();
	}
}
