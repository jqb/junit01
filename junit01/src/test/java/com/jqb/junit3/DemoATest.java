package com.jqb.junit3;

import com.jqb.junit3.DemoA;

import junit.framework.Assert;
import junit.framework.TestCase;

/**
 * 在junit3中,测试方法需要满足如下原则 1.public 2.void 3.无方法参数 4.方法名必须以test开头
 * 
 */
public class DemoATest extends TestCase {

	DemoA demoA = null;
	int a = 1, b = 0;
	/***
	 * 带参构造是为配合重复测试,见TestRepeatedly类
	 * @param functionName
	 */
	public DemoATest(String functionName){
		super(functionName);
	}
	/**
	 * 每个测试用例之前调用
	 */
	@Override
	protected void setUp() throws Exception {
		demoA = new DemoA();
		System.out.println("setUp" + demoA.toString());
	}

	/**
	 * 每个测试用例之后调用
	 */
	@Override
	protected void tearDown() throws Exception {
		System.out.println("tearDown" + demoA.toString());
	}

	/**
	 * 最好是[test+原方法名]来命名测试方法
	 */
	public void testAdd() {

		int result = demoA.add(a, b);
		/**
		 * 第一个参数是期望值:常量 ;第二个参数是变量
		 */
		Assert.assertEquals(1, result);
	}

	public void testSubtract() {
		int result = demoA.subtract(a, b);
		Assert.assertEquals(1, result);
	}

	public void testMultiply() {
		int result = demoA.multiply(a, b);
		Assert.assertEquals(0, result);
	}
//   /**
//    * 方法一
//	*/
//	public void testDivide() {
//		int result = 0;
//		try {
//			result = demoA.divide(a, b);
//		} catch (Exception e) {
//			e.printStackTrace();
//			/**
//			 * 处理测试过程中的发生的异常,不加下面这行代码,如果除数为零测试也会通过
//			 * 只是控制台会打印出异常信息,而junit是通过的,所以要告诉junit,如果发生了异常,测试应该是失败的
//			 */
//			Assert.fail("测试失败");
//		}
//		Assert.assertEquals(0, result);
//	}
	/**
	 * 方法二 
	 */
	public void testDivide() {
		Throwable testException = null;
		
		try {
			demoA.divide(a, b);
			Assert.fail();
		} catch (Exception e) {
			testException = e;
			System.out.println(e.getMessage());
		}
		Assert.assertNotNull(testException);
		Assert.assertEquals(Exception.class, testException.getClass());
		Assert.assertEquals("除数不能为0",testException.getMessage());
	}
	
}
