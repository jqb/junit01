package com.jqb.junit3;

import java.lang.reflect.Method;

import junit.framework.Assert;
import junit.framework.TestCase;

public class DemoETest extends TestCase {

	/***
	 * 使用反射来测试私有方法
	 */
	public void testAdd(){
		Class<DemoE> clazz = DemoE.class;
		/**
		 * Integer.TYPE  -  int
		 * int.class     -  int
		 * Integer.class -  Integer
		 */
		Method method = null;
		try {
			//add方法有2个int参数,所以使用Class数组表示,(new Class[]{int.class,int.class}也可以)
			method = clazz.getDeclaredMethod("add", new Class[]{Integer.TYPE,int.class});
			//压制访问权限
			method.setAccessible(true);
			Object result = method.invoke(clazz.newInstance(), new Object[]{2,3});
			Assert.assertEquals(5,result);
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail("测试失败");
		} 
		
	}
}
