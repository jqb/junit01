package com.jqb.junit3;

import junit.extensions.RepeatedTest;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

public class TestRepeatedly extends TestCase{

	public static Test suite(){
		/***
		 * 测试套件
		 */
		TestSuite suite = new TestSuite();
		/***
		 * 重复测试DemoATest测试类的testAdd测试方法10次
		 */
		suite.addTest(new RepeatedTest(new DemoATest("testAdd"),10));
		return suite;
	}
	
	
}
