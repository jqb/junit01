package com.jqb.junit3;

/***
 * 模拟堆栈
 * 
 * @author Dong
 * 
 */
public class DemoC {
	/***
	 * currentIndex为当前指针位置[栈顶元素之前]
	 */
	private int currentIndex = 0;
	/***
	 * 栈大小
	 */
	private int size = 0;
	/***
	 * 栈
	 */
	private String[] stack = null;
	/***
	 * 构造函数初始化栈
	 */
	public DemoC(){
		stack = new String[100];
		size = stack.length;
	}
	public void push(String value) throws Exception{
		if(currentIndex == size){
			throw new Exception("已至栈顶");
		}
		stack[currentIndex++] = value;
	}
	public String pop() throws Exception{
		if(currentIndex == 0){
			throw new Exception("已至栈底");
		}
		return stack[--currentIndex];
	}
	public int delete(int n) throws Exception{
		if(n>currentIndex){
			throw new Exception("栈中元素不足");
		}
		currentIndex = currentIndex-n;
		return currentIndex;
	}
	public String top() throws Exception{
		if(currentIndex == 0){
			throw new Exception("栈是空的");
		}
		return stack[currentIndex-1];
	}
}
