package com.jqb.junit3;

public class DemoA {
	
	public int add(int a, int b) {
		return a + b;
	}

	public int subtract(int a, int b) {
		return a - b;
	}

	public int multiply(int a, int b) {
		return a * b;
	}

	public int divide(int a, int b) throws Exception {
		if (b == 0) {
			throw new Exception("除数不能为0");
		}
		return a / b;
	}

	@Override
	public String toString() {
		return "I'm DemoA...";
	}
	public static void main(String[] args) throws Exception {
		DemoA ta = new DemoA();
		int a = 1, b = 2;
		int add = ta.add(a, b);
		int sub = ta.subtract(a, b);
		int mul = ta.multiply(a, b);
		int div = ta.divide(a, b);
		/**
		 * 原始验证方式
		 */
		System.out.println(a + "+" + b + "=" + add);
		System.out.println(a + "-" + b + "=" + sub);
		System.out.println(a + "*" + b + "=" + mul);
		System.out.println(a + "/" + b + "=" + div);
	}
	
}
