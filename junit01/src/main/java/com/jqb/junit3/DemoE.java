package com.jqb.junit3;

public class DemoE {
	
	/***
	 * 私有方法
	 * @param a
	 * @param b
	 * @return
	 */
	@SuppressWarnings("unused")
	private int add(int a, int b) {
		return a + b;
	}
}
